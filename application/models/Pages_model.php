<?php
class Pages_model extends CI_Model{
	public function __construct()
    {
      parent::__construct();
	  $this->load->database();
    }

	public function get_academic_level(){
        $this->db->order_by('academic_level.academic_level_id', 'DESC');
		$query = $this->db->get('academic_level');
				
        return $query->result_array();
    }
    
    public function get_noofdays(){
        $this->db->order_by('noofdays.noofdays_id', 'ASC');
		$query = $this->db->get('noofdays');
		
        return $query->result_array();
    }
    
    public function get_noofpages(){
        $this->db->order_by('noofpages.noofpages_id', 'ASC');
		$query = $this->db->get('noofpages');

        return $query->result_array();
    }
    
    public function get_typesofpaper(){
        $this->db->order_by('typesofpaper.typeofpaper_id', 'ASC');
		$query = $this->db->get('typesofpaper');
		
        return $query->result_array();
	}
	
}