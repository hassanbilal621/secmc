<?php
class User_model extends CI_Model{
	public function __construct()
    {
      parent::__construct();
	  $this->load->database();
    }
	

    public function insert_data($product_image){
        $data = array(
            'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'cnic' => $this->input->post('cnic'),
			'email' => $this->input->post('email'),
			'cnic_photo' => $product_image,
			'phone' => $this->input->post('phone'),
			'hosted_by_name'=> $this->input->post('hosted_by_name'),
			'phone_no_of_the_host'=> $this->input->post('phone_no_of_the_host'),
			'date_of_visit'=> $this->input->post('date_of_visit'),
			'time_of_visit'=> $this->input->post('time_of_visit'),
			'purpose_of_visit'=> $this->input->post('purpose_of_visit'),
			'vehicle_no'=> $this->input->post('vehicle_no'),
			'status'=> 'pending',
        );

		$this->security->xss_clean($data);
		$this->db->insert('form', $data);
		return $this->db->insert_id();
	}

	public function morevisitor($imgname){
		$data = array(
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'cnic' => $this->input->post('cnic'),
			'email' => $this->input->post('email'),
			'cnic_photo' => $imgname,
			'status' => 'pending'

		);
		$this->security->xss_clean($data);
        return $this->db->insert('more_visitor', $data);
	}

	public function get_visitor(){

		$this->db->where('status', 'pending');
		$query = $this->db->get('more_visitor');
		return $query->result_array();
	}

	public function deletevisitor($visitorid){

		$this->db->where('visitor_id', $visitorid);
		$this->db->delete('more_visitor');
	}

	public function get_search($formnum){

		$this->db->where('id', $formnum);
		$query = $this->db->get('form');
		return $query->row_array();
	}


	public function uploadimage($imgurl){
		$data =array(
			'imgurl' => $imgurl
		);
		$this->security->xss_clean($data);
		return $this->db->insert('images',$data);

	}

}


