<?php
class Admin_model extends CI_Model
{

	public function login($username, $password)
	{

		$this->db->where('username', $username);
		$result = $this->db->get('admin');

		if ($result->num_rows() == 1) {
			$hash = $result->row(0)->password;

			if (password_verify($password, $hash)) {
				return $result->row(0)->id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function submit_form($cnicphoto, $visitorphoto)
	{
		$data = array(
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'cnic' => $this->input->post('cnic'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'hosted_by_name' => $this->input->post('hosted_by_name'),
			'phone_no_of_the_host' => $this->input->post('phone_no_of_the_host'),
			'date_of_visit' => $this->input->post('date_of_visit'),
			'time_of_visit' => $this->input->post('time_of_visit'),
			'purpose_of_visit' => $this->input->post('purpose_of_visit'),
			'vehicle_no' => $this->input->post('vehicle_no'),
			'cnic_photo' => $cnicphoto,
			'visitor_photo' => $visitorphoto,
			'status' => 'pending'

		);
		$this->security->xss_clean($data);
		$this->db->insert('form', $data);

		return $this->db->insert_id();
	}

	//staff / Add / Update / Delete  start

	public function addstaff($imgname, $enc_password)
	{
		$data = array(
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'image' => $imgname,
			'username' => $this->input->post('username'),
			'password' => $enc_password,
			'cnic' => $this->input->post('cnic'),
			'number' => $this->input->post('number'),
			'salary' => $this->input->post('salary'),
			'Fingerno' => $this->input->post('Fingerno'),
			'role' => $this->input->post('role')
		);

		$this->security->xss_clean($data);
		$this->db->insert('staff', $data);
	}

	public function get_staff()
	{



		$query = $this->db->get('staff');
		return $query->result_array();
	}

	public function update_staff($imgname,  $staffid)
	{
		$data = array(
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'image' => $imgname,
			'username' => $this->input->post('username'),
			'cnic' => $this->input->post('cnic'),
			'number' => $this->input->post('number'),
			'salary' => $this->input->post('salary'),
			'Fingerno' => $this->input->post('Fingerno'),
			'role' => $this->input->post('role')
		);

		$this->security->xss_clean($data);
		$this->db->where('staff_id', $staffid);
		$this->db->update('staff', $data);
	}

	public function del_staff($staffid)
	{
		$this->db->where('staff_id', $staffid);
		$this->db->delete('staff');
	}


	//staff / Add / Update / Delete  End

	public function get_form_pending()
	{
		$this->db->where('status', 'pending');
		$query = $this->db->get('form');
		return $query->result_array();
	}

	public function count_form_pending()
	{
		$this->db->where('status', 'pending');
		$query = $this->db->get('form');
		return $query->num_rows();
	}
	
	public function get_form()
	{
		$query = $this->db->get('form');
		return $query->result_array();
	}

	public function approved($formid)
	{
		$data = array(
			'status' => 'approved'
		);
		$this->db->where('id', $formid);
		$this->db->update('form', $data);
	}

	public function get_ajax_form($formid)
	{
		$this->db->where('id', $formid);
		$query = $this->db->get('form');
		return $query->row_array();
	}
	public function get_ajax_staff($staffid)
	{
		$this->db->where('staff_id', $staffid);
		$result = $this->db->get('staff');
		return $result->row_array();
	}


	public function get_visitor_formid($formid)
	{
		$this->db->where('formid', $formid);
		$query = $this->db->get('form_visitor');
		return $query->result_array();
	}

	public function del_form($formid)
	{
		$this->db->where('id', $formid);
		$this->db->delete('form');
	}
	public function del_form_visitors($formid)
	{
		$this->db->where('formid', $formid);
		$this->db->delete('form_visitor');
	}
}
