<?php
class Users extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library('cart');
    }


    
    public function submitrequest(){
    {
     


        $this->form_validation->set_rules('firstname', 'firstname', 'required');
        $this->form_validation->set_rules('lastname', 'lastname', 'required');
        $this->form_validation->set_rules('cnic', 'cnic', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('hosted_by_name', 'hosted_by_name', 'required');
        $this->form_validation->set_rules('phone_no_of_the_host', 'phone_no_of_the_host', 'required');
        $this->form_validation->set_rules('date_of_visit', 'date_of_visit', 'required');
        $this->form_validation->set_rules('time_of_visit', 'time_of_visit', 'required');
        $this->form_validation->set_rules('purpose_of_visit', 'purpose_of_visit', 'required');
        $this->form_validation->set_rules('vehicle_no', 'vehicle_no', 'required');


        if (empty($_FILES['cnicphoto']['name']))
        {
            $this->form_validation->set_rules('cnicphoto', 'Document', 'required');
        }

        if (empty($_FILES['visitorphoto']['name']))
        {
            $this->form_validation->set_rules('visitorphoto', 'Document', 'required');
        }

        

        if ($this->form_validation->run() === FALSE) {


            redirect('users/index/?=formvalidationfailed');


        } else {

            
            $cnicphoto = $this->do_upload1('cnicphoto');
            $visitorphoto = $this->do_upload1('visitorphoto');


            $formid = $this->admin_model->submit_form($cnicphoto, $visitorphoto);


            $number = count($_POST["visitor_firstname"]);
            if ($number > 1) {
                for ($i = 0; $i < $number; $i++) {
                    if (trim($_POST["visitor_firstname"][$i] != '')) {


                        $visitorphoto = $this->do_upload2($i);

                        $this->add_visitor_form($formid, $_POST["visitor_firstname"][$i], $_POST["visitor_lastname"][$i], $_POST["visitor_cnic"][$i], $_POST["visitor_email"][$i], $_POST["visitor_phone"][$i], $visitorphoto);
                    }
                }
                echo "Data Inserted";
            }

        }

        }
    }


    public function add_visitor_form($formid, $visitor_firstname, $visitor_lastname, $visitor_cnic, $visitor_email, $visitor_phone, $visitorphoto)
    {

        $data = array(
            'firstname' => $visitor_firstname,
            'lastname' => $visitor_lastname,
            'cnic' => $visitor_cnic,
            'email' => $visitor_email,
            'visitor_phone' => $visitor_phone,
            'visitorphoto' => $visitorphoto,
            'formid' => $formid
        );

        $this->security->xss_clean($data);
        $this->db->insert('form_visitor', $data);
        echo "more Visitor Inserted";
    }
  
    function do_upload1($imgname)
    {

        $config = array(
            'upload_path' => "assets/uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => false,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            //'max_height' => "5000",
            //'max_width' => "5000"
        );

        $this->load->library('upload', $config);

        if ($this->upload->do_upload($imgname)) {
            $imgdata = array('upload_data' => $this->upload->data());
            $imgname = $imgdata['upload_data']['file_name'];
        } else {
            $error = array('error' => $this->upload->display_errors());
            echo '<pre>';
            print_r($error);
            echo '<pre>';
            exit;
        }

        return $imgname;
    }



    function do_upload2($i)
    {

        $config = array(
            'upload_path' => "assets/uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => false,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            //'max_height' => "5000",
            //'max_width' => "5000"
        );

        $_FILES['file']['name']       = $_FILES['visitor_photo']['name'][$i];
        $_FILES['file']['type']       = $_FILES['visitor_photo']['type'][$i];
        $_FILES['file']['tmp_name']   = $_FILES['visitor_photo']['tmp_name'][$i];
        $_FILES['file']['error']      = $_FILES['visitor_photo']['error'][$i];
        $_FILES['file']['size']       = $_FILES['visitor_photo']['size'][$i];

        // Load and initialize upload library
        $this->load->library('upload', $config);
        $this->upload->initialize($config);


        if ($this->upload->do_upload('file')) {
            $imgdata = array('upload_data' => $this->upload->data());
            $imgname = $imgdata['upload_data']['file_name'];
        } else {
            $error = array('error' => $this->upload->display_errors());
            echo '<pre>';
            print_r($error);
            echo '<pre>';
            exit;
        }

        return $imgname;
    }


    function do_upload()
    {

        $config = array(
            'upload_path' => "assets/uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => false,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "5000",
            'max_width' => "5000"
        );

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {
            $imgdata = array('upload_data' => $this->upload->data());
            $imgname = $imgdata['upload_data']['file_name'];
        } else {
            $error = array('error' => $this->upload->display_errors());
            echo '<pre>';
            print_r($error);
            echo '<pre>';
            exit;
        }

        return $imgname;
    }


  
    public function index()
    {
        //$data['visitors'] = $this->user_model->get_visitor();
        $data['title'] = 'SECMC';

        $this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php');
        $this->load->view('templates/users/index.php', $data);
        $this->load->view('templates/users/footer.php');
    }

    public function search()
    {

        $this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php');
        $this->load->view('templates/users/search.php');
        $this->load->view('templates/users/footer.php');
    }


    public function search_result()
    {
        $formnum = $this->input->post('formno');

        $data['search'] = $this->user_model->get_search($formnum);
        $data['Visitors'] = $this->admin_model->get_visitor_formid($formnum);
        if (isset($data['search'])) {


            $this->load->view('templates/users/header.php');
            $this->load->view('templates/users/navbar.php');
            $this->load->view('templates/users/search_result.php', $data);
            $this->load->view('templates/users/footer.php');
        } else {
            redirect('users/search');
        }
    }

    public function morevisitor()
    {
        $this->form_validation->set_rules('firstname', 'firstname', 'required');


        if ($this->form_validation->run() === FALSE) {

            redirect('users/?=insert_feild');
        } else {

            $imgname = $this->do_upload();

            $this->user_model->morevisitor($imgname);

            redirect('users/');
        }
    }
    public function delete_visitor($visitorid)
    {
        $this->user_model->deletevisitor($visitorid);
        redirect('users/');
    }


}
