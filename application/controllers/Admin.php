<?php
class admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
    }

    public function login()
    {

        if ($this->session->admindata('secmc_admin_id')) {
            redirect('admin/');
        }

        $data['title'] = 'Nexco Japan';

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === FALSE) {

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/login.php', $data);
        } else {

            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $secmc_admin_id = $this->admin_model->login($username, $password);

            if ($secmc_admin_id) {
                $admin_data = array(
                    'secmc_admin_id' => $secmc_admin_id,
                    'japan_email' => $username,
                    'submit_alogged_in' => true
                );
                $this->session->set_admindata($admin_data);

                redirect('admin/');
            } else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('admin/login');
            }
        }
    }

    public function logout()
    {

        $this->session->unset_userdata('secmc_admin_id');
        $this->session->unset_userdata('japan_email');
        $this->session->unset_userdata('submit_alogged_in');

        $this->session->set_flashdata('admin_loggedout', 'You are now logged out');
        redirect('admin/login');
    }

    function do_upload()
    {

        $config = array(
            'upload_path' => "assets/uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => false,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "5000",
            'max_width' => "5000"
        );

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {
            $imgdata = array('upload_data' => $this->upload->data());

            $imgname = $imgdata['upload_data']['file_name'];
        } else {
            $error = array('error' => $this->upload->display_errors());
            echo '<pre>';
            print_r($error);
            echo '<pre>';
            exit;
        }

        return $imgname;
    }

    public function index()
    {
        if (!$this->session->admindata('secmc_admin_id')) {
            redirect('admin/login');
        }
        $data['forms'] = $this->admin_model->get_form_pending();
        $data['counter'] = $this->admin_model->count_form_pending();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/navbar.php', $data);
        $this->load->view('templates/admin/index.php', $data);
        $this->load->view('templates/admin/footer.php');
    }
    public function viewform($formid)
    {
        if (!$this->session->admindata('secmc_admin_id')) {
            redirect('admin/login');
        }
        $data['form'] = $this->admin_model->get_ajax_form($formid);
        $data['Visitors'] = $this->admin_model->get_visitor_formid($formid);
        $data['counter'] = $this->admin_model->count_form_pending();
      

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/navbar.php', $data);
        $this->load->view('templates/admin/viewform.php', $data);
        $this->load->view('templates/admin/footer.php');
    }
    public function approved($formid)
    {
        $this->admin_model->approved($formid);

        redirect('admin/index');
    }


    // staff Order Manager Start


    public function addstaff()
    {
        if (!$this->session->admindata('secmc_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('firstname', 'firstname', 'required');


        if ($this->form_validation->run() === FALSE) {
            $data['counter'] = $this->admin_model->count_form_pending();


            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php', $data);
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/addstaff.php');
            $this->load->view('templates/admin/footer.php');
        } else {
            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

            $imgname = $this->do_upload();

            $this->admin_model->addstaff($imgname, $enc_password);

            redirect('admin/addstaff');
        }
    }

    public function managestaff()
    {
        if (!$this->session->admindata('secmc_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('firstname', 'firstname', 'required');

        if ($this->form_validation->run() === FALSE) {


            $data['staffs'] = $this->admin_model->get_staff();
            $data['counter'] = $this->admin_model->count_form_pending();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php', $data);
            $this->load->view('templates/admin/aside.php', $data);
            $this->load->view('templates/admin/managestaff.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            if (!file_exists($_FILES["userfile"]["tmp_name"])) {
                $staffid = $this->input->post('staffid');
                $currUser = $this->admin_model->get_ajax_staff($staffid);

                $imgname = $currUser["image"];
                $this->admin_model->update_staff($imgname,  $staffid);
            } else {
                $imgname = $this->do_upload();
                $productid = $this->input->post('staffid');
                $this->admin_model->update_staff($imgname,  $staffid);
            }

            redirect('admin/managestaff');
        }
    }

    public function staffdelete($staffid)
    {

        $this->admin_model->del_staff($staffid);
        redirect('admin/managestaff');
    }

    // staff Order Manager End


    public function forms()
    {
        if (!$this->session->admindata('secmc_admin_id')) {
            redirect('admin/login');
        }
        $data['forms'] = $this->admin_model->get_form();
        $data['counter'] = $this->admin_model->count_form_pending();


        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php', $data);
        $this->load->view('templates/admin/aside.php', $data);
        $this->load->view('templates/admin/forms.php', $data);
        $this->load->view('templates/admin/footer.php');
    }
    public function ajax_edit_staff_adminmodal($staffid)
    {

        $data['staff'] = $this->admin_model->get_ajax_staff($staffid);

        $this->load->view('templates/ajax/editstaff.php', $data);
    }

    public function ajax_edit_form_adminmodal($formid)
    {
        $data['form'] = $this->admin_model->get_ajax_form($formid);
        $data['Visitors'] = $this->admin_model->get_visitor_formid($formid);

        $this->load->view('templates/ajax/viewform.php', $data);
    }

    public function formdelete($formid)
    {

        $this->admin_model->del_form($formid);
        $this->admin_model->del_form_visitors($formid);
        redirect('admin/forms');
    }
}
