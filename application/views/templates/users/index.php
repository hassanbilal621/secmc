<div id="main" class="mt-2">
   <div class="row">
      <div class="col s10 ml-10 ">
         <div class="card-content br-1 z-depth-5" style="border: 2px black solid">

            <div class="row mt-1">
               <div class="col s6 l6 left ">
                  <h4 class="normalheading">Visitor form</h4>
               </div>
               <div class="col s6 l6 left ">
                  <a class="waves-effect waves-light  btn z-depth-5  submit-1 box-shadow-none border-round mr-1 mb-1 right" href="<?php echo base_url(); ?>users/search" type="submit" name="action">Search Your Form
                     <i class="material-icons left">search</i>
                  </a>
               </div>
            </div>
            <?php echo form_open_multipart('users/submitrequest'); ?>
            <div class="row">
               <div class="col s11 ml-3">
                  <div class="row">
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>First Name</h6>
                           <input type="text" name="firstname" placeholder="Type First Name">
                        </div>
                     </div>
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>Last Name</h6>
                           <input type="text" name="lastname" placeholder="Type Last Name">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>CNIC</h6>
                           <input type="number" name="cnic" placeholder="Type CNIC">
                        </div>
                     </div>
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>Email</h6>
                           <input type="email" name="email" placeholder="Type Email">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>CNIC Photo</h6>
                           <input type="file" id="input-file-now" class="dropify" data-default-file="" name="cnicphoto" accept="image/*" />
                        </div>
                     </div>
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>Visitor Photo</h6>
                           <input type="file" id="input-file-now" class="dropify" data-default-file="" name="visitorphoto" accept="image/*" />
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>Phone</h6>
                           <input type="text" name="phone" placeholder="Type Phone">
                        </div>
                     </div>
                  </div>
                  <h5 class="normalheading">Hosted form</h5>
                  <div class="row">
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>Hosted by Name</h6>
                           <input type="text" name="hosted_by_name" placeholder="Type Hosted by Name">
                        </div>
                     </div>
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>Phone No. of the Host</h6>
                           <input type="text" name="phone_no_of_the_host" placeholder="Type Phone No. of the Host">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>Date of Visit</h6>
                           <input type="date" name="date_of_visit" placeholder="Type Date of Visit">
                        </div>
                     </div>
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>Time of Visit</h6>
                           <input type="time" name="time_of_visit" placeholder="Type Time of Visit">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>Purpose of Visit</h6>
                           <input type="text" name="purpose_of_visit" placeholder="Type Purpose  of Visit">
                        </div>
                     </div>
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>Vehicle No</h6>
                           <input type="text" name="vehicle_no" placeholder="Type Vehicle No">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col s12">

                        <h5 class="normalheading">More Visitors</h5>
                        <div id="tab_logic">

                           <div id="dynamic_field">
                           </div>
                           <div class="input-field col s2">
                              <a id="add_row" class="waves-effect waves-light  btn edit box-shadow-none border-round mr-1 mb-1" >Add More Visitor
                                 <i class="material-icons left">person</i></a>
                           </div>
                        </div>
                     </div>
                  </div>
         
                  <div class="row">
                     <div class="col s12">
                        <div class="input-field col s12">
                           <button class="waves-effect waves-light z-depth-5 btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">Save
                              <i class="material-icons right">save</i>
                           </button>
                        </div>
                     </div>
                  </div>
               </div>

               <!-- <button class="waves-effect waves-light  btn edit box-shadow-none border-round mr-1 mb-1 modal-trigger" type="submit" href="#modal3" name="action">Add More Visitor
                  <i class="material-icons left">person</i>
               </button> -->
            </div>

            <?php echo form_close(); ?>
         </div>
      </div>
   </div>
</div>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<script>
   $(document).ready(function() {
      var visitorid = 1;
      $('#add_row').click(function() {

         visitorid++;
         $('#dynamic_field').append('<div id="row'+visitorid+'"><div class="row"><div class="col m12 l6 s12"><div class="input-field col s12"><h6>First Name</h6><input type="text" name="visitor_firstname[]" value="" placeholder="Type First Name"></div></div><div class="col m12 l6 s12"><div class="input-field col s12"><h6>Last Name</h6> <input type="text" name="visitor_lastname[]" value="" placeholder="Type Last Name"> </div></div ></div><div class="row"><div class="col m12 l6 s12"><div class="input-field col s12"><h6>CNIC</h6><input type="number" name="visitor_cnic[]" value="" placeholder="Type CNIC"></div></div><div class="col m12 l6 s12"><div class="input-field col s12"><h6>Email</h6><input type="email" name="visitor_email[]" value="" placeholder="Type Email"></div></div></div><div class="row"><div class="col m12 l6 s12"><div class="input-field col s12"><h6>CNIC Photo</h6> <input type="file" id="input-file-now" class="dropify" data-default-file="" name="visitor_photo[]" accept="image/*" /></div></div><div class="col m12 l6 s12"><div class="input-field col s12"><h6>Phone</h6><input type="text" name="visitor_phone[]" value="" placeholder="Type Phone"></div></div></div><div class="row"><div class="col m12 l6 s12"><a name="remove" id="'+visitorid+'" class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1  btn submit box-shadow-none border-round mr-1 mb-1 right btn_remove">Delete Row</a></div></div></div>');
      });
      $(document).on('click', '.btn_remove', function() {
         var button_id = $(this).attr("id");
         // alert(button_id);
         $('#row'+button_id).remove();
      });
   });
</script>