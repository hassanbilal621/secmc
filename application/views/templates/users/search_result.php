<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="row">
            <div class="col s6 l6 left ">
               <h5 class="normalheading">Visitor form</h5>
            </div>
            <div class="col s6 l6 left ">
               <a class="waves-effect waves-light  btn  submit box-shadow-none border-round mr-1 mb-1 right" href="<?php echo base_url(); ?>users/search" type="submit" name="action">Search Your Form
                  <i class="material-icons left">search</i>
               </a>
            </div>
         </div>
         <div class="row">
            <div class="col s12">
               <div class="row">
                  <div class="col m12 l6 s12">
                     <div class="input-field col s12">
                        <h6>Full Name</h6>
                        <input type="text" name="firstname" placeholder="Type Full Name" value="<?php echo $search['firstname']; ?>" readonly>
                     </div>
                  </div>
                  <div class="col m12 l6 s12">
                     <div class="input-field col s12">
                        <h6>LAst Name</h6>
                        <input type="text" name="lastname" placeholder="Type name" value="<?php echo $search['lastname']; ?>" readonly>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col m12 l6 s12">
                     <div class="input-field col s12">
                        <h6>CNIC</h6>
                        <input type="number" name="cnic" placeholder="Type name" value="<?php echo $search['cnic']; ?>" readonly>
                     </div>
                  </div>
                  <div class="col m12 l6 s12">
                     <div class="input-field col s12">
                        <h6>Email</h6>
                        <input type="email" name="email" placeholder="Type Email" value="<?php echo $search['email']; ?>" readonly>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col m12 l6 s12">
                     <div class="input-field col s12">
                        <h6>CNIC Photo</h6>
                        <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $search['cnic_photo']; ?>" />
                     </div>
                  </div>
                  <div class="col m12 l6 s12">
                     <div class="input-field col s12">
                        <h6>Visitor Photo</h6>
                        <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $search['visitor_photo']; ?>" />
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col m12 l6 s12">
                     <div class="input-field col s12">
                        <h6>Phone</h6>
                        <input type="number" name="phone" placeholder="Type Phone" value="<?php echo $search['phone']; ?>" readonly>
                     </div>
                  </div>
               </div>
               <h5 class="normalheading">Hosted form</h5>

               <div class="row">
                  <div class="col m12 l6 s12">
                     <div class="input-field col s12">
                        <h6>Hosted by Name</h6>
                        <input type="text" name="hosted_by_name" placeholder="Type Hosted by Name" value="<?php echo $search['hosted_by_name']; ?>" readonly>
                     </div>
                  </div>
                  <div class="col m12 l6 s12">
                     <div class="input-field col s12">
                        <h6>Phone No. of the Host</h6>
                        <input type="text" name="phone_no_of_the_host" placeholder="Type Phone No. of the Host" value="<?php echo $search['phone_no_of_the_host']; ?>" readonly>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col m12 l6 s12">
                     <div class="input-field col s12">
                        <h6>Date of Visit</h6>
                        <input type="text" name="date_of_visit" placeholder="Type Date of Visit" value="<?php echo $search['date_of_visit']; ?>" readonly>
                     </div>
                  </div>
                  <div class="col m12 l6 s12">
                     <div class="input-field col s12">
                        <h6>Time of Visit</h6>
                        <input type="text" name="time_of_visit" placeholder="Type Time of Visit" value="<?php echo $search['time_of_visit']; ?>" readonly>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col m12 l6 s12">
                     <div class="input-field col s12">
                        <h6>Purpose of Visit</h6>
                        <input type="text" name="purpose_of_visit" placeholder="Type Purpose  of Visit" value="<?php echo $search['purpose_of_visit']; ?>" readonly>
                     </div>
                  </div>
                  <div class="col m12 l6 s12">
                     <div class="input-field col s12">
                        <h6>Vehicle No</h6>
                        <input type="text" name="vehicle_no" placeholder="Type Vehicle No" value="<?php echo $search['vehicle_no']; ?>" readonly>
                     </div>
                  </div>
               </div>
               <?php $count = 01 ?>
               <?php foreach ($Visitors as $Visitor) : ?>
                  <h5 class="normalheading">Visitor-0<?php echo $count ?></h5>
                  <div class="row">
                     <div class="col m12 l6 s12">
                        <div class="col input-field s12">
                           <h6>Visitor Photo</h6>
                           <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $Visitor['visitorphoto']; ?>" style="max-height: 210px;" />
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>Full Name</h6>
                           <input type="text" name="firstname" placeholder="Type Full Name" value="<?php echo $Visitor['firstname']; ?>" readonly>
                        </div>
                     </div>
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>LAst Name</h6>
                           <input type="text" name="lastname" placeholder="Type name" value="<?php echo $Visitor['lastname']; ?>" readonly>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>CNIC</h6>
                           <input type="number" name="cnic" placeholder="Type name" value="<?php echo $Visitor['cnic']; ?>" readonly>
                        </div>
                     </div>
                     <div class="col m12 l6 s12">
                        <div class="input-field col s12">
                           <h6>Email</h6>
                           <input type="email" name="email" placeholder="Type Email" value="<?php echo $Visitor['email']; ?>" readonly>
                        </div>
                     </div>
                  </div>
               <?php $count++;
               endforeach; ?>
            </div>
         </div>
      </div>
   </div>
</div>
</div>