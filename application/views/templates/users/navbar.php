
<header class="page-topbar" id="header">
  <div class="navbar navbar-fixed">
    <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark blackgrade">
      <div class="nav-wrapper">
        <ul class="left">
          <li>
            <h1 class="logo-wrapper">
              <a class="brand-logo darken-1 pl-20" href="<?php echo base_url(); ?>users/"><img src="<?php echo base_url(); ?>/assets/app-assets/images/logo/logo.png" alt="Optv Globle" class="logoimg">
                <span class="logo-text  secmc">Sindh Engro Coal Mining Company (SECMC) is operating Pakistan’s first </span></a>
            </h1>
          </li>
        </ul>
        <div class="">
          <ul class="right " id="ul-horizontal-nav" data-menu="menu-navigation">
            
            <li class="bold">
              <img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/login.png" alt="" class="navicon">
              <a class="ml-07 waves-effect waves-cyan ho_ac navtext right" href="<?php echo base_url(); ?>admin/">
                <span class="menu-title " data-i18n="">log In</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
</header>