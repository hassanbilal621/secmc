<div id="main">
   <div class="row">
      <div class="col s10 mt-10" style="padding-left: 20%;">
            <div class="card-content br-1 z-depth-5"style="border: 2px black solid">>
               <h4 class="normalheading">Search Your Form</h4>
               <?php echo form_open('users/search_result'); ?>
               <div class="row">
                  <div class="col s11 ml-3 " >
                  <input type="text" name="formno" placeholder="Type Form Number">
                     <button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mt-1 mb-1 right" type="submit" name="action">serach
                        <i class="material-icons left">search</i>
                     </button>
                  </div>
               </div>
               <?php echo form_close(); ?>
            </div>
         </div>
      </div>
   </div>
</div>
