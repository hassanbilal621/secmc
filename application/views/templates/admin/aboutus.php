<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="card br-1">
				<div class="card-content">
					<h4 class="card-title">About Us</h4>
					<?php echo form_open_multipart('admin/update_about'); ?>
						<div class="row">
							<div class="col s12">
								<div class="row">
									<div class="col s6">
										<div class="input-field col s12">
											<h6>Name</h6>
											<input  type="text" name="name" value="<?php echo $abouts['name']; ?>">
										</div>
										<div class="input-field col s12">
											<h6>contact 01</h6>
											<input type="number" name="contact-01" value="<?php echo $abouts['contact-01']; ?>" >
										</div>
                              <div class="input-field col s12">
											<h6>contact 02</h6>
											<input type="number" name="contact-02" value="<?php echo $abouts['contact-02']; ?>">
										</div>
									</div>
									<div class="col s6">
										<div class="input-field col s12">
											<h6>News</h6>
											<textarea cols="30" rows="10"  placeholder="Type News" class="discrip" id="news" type="text" name="aboutus"style="height: 275px;" ><?php echo $abouts['aboutus']; ?></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col s12">
										<div class="input-field col s12">
											<button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">News Post
											<i class="material-icons right">send</i>
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
