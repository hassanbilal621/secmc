<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="card br-1">
				<div class="card-content">
					<h4 class="card-title">Lastest latestnews</h4>
					<?php echo form_open_multipart('admin/addlatestlatestnews'); ?>
					<div class="row">
						<div class="col s2 right">
							<h6>Date</h6>
							<input type="text" class="datepicker" name="latestlatestnewsdate" placeholder=" Type Date" required>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<div class="row">
								<div class="col s6">
									<div class="input-field col s12">
										<h6>latestnews Title</h6>
										<input id="latestnewstitle" type="text" name="latestlatestnewstitle">
									</div>
									<div class="input-field col s12">
										<h6>latestnews url</h6>
										<input id="url" type="url" name="latestlatestnewsurl">
									</div>
								</div>
								<div class="col s6">
									<div class="input-field col s12">
										<h6>latestnews</h6>
										<textarea cols="30" rows="10" placeholder="Type latestnews" class="discrip" id="latestnews" type="text" name="latestnews"></textarea>
									</div>
								</div>
							</div>
							<div class="col s6">
								<div class="input-field col s12">
									<h6>Live Thumnail</h6>
									<input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<div class="input-field col s12">
										<button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">latestnews Post
											<i class="material-icons right">send</i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php echo form_close(); ?>

					<div class="row" style="padding: 0 40px 0 20px;">
						<table id="page-length-option" class="display">
							<thead>
								<tr>
									<th>latestnews Id</th>
									<th>latestnews Title</th>
									<th>latestnews URL</th>
									<th>latestnews Discruption</th>
									<th>action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($latestnewses as $latestnews) : ?>
									<tr>
										<td style="width: 1%;"><?php echo $latestnews['latestnews_id']; ?></td>
										<td><?php echo $latestnews['latestnewstitle']; ?></td>
										<td><iframe src="https://www.youtube.com/embed/<?php echo $latestnews['latestnewsurl'];  ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 60%;height: 100%;"></iframe>
										</td>
										<td style="width: 15% !important;"><?php echo $latestnews['news']; ?></td>
										<td>
											<button class="waves-effect waves-light  btn edit box-shadow-none border-round mr-1 mb-1 modal-trigger" onclick="loadnewsinfo(this.id)" id="<?php echo $latestnews['latestnews_id']; ?>" type="submit" href="#modal3" name="action">EDIT
												<i class="material-icons left">edit</i>
											</button>
											<a class="waves-effect waves-light  btn delete box-shadow-none border-round mr-1 mb-1" href="<?php echo base_url(); ?>admin/deletelatestnews/<?php echo $latestnews['latestnews_id']; ?>" type="submit" name="action">Delete
												<i class="material-icons left">delete</i>
											</a>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<script>
	$(document).ready(function() {
		$('.datepicker').datepicker();
	});
</script>

<div id="modal3" class="modal">
   <div class="modal-content">
   </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>
   function loadnewsinfo(newsid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_edit_latestnews_adminmodal/" + newsid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>