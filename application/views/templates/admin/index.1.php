<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="chat-application">
                <div class="app-chat">
                    <div class="content-area content-right">
                        <div class="app-wrapper">
                            <div class=" card-default scrollspy border-radius-6 fixed-width">
                                <div class="card-content chat-content p-0">
                                    <div class="sidebar-left bb-1  mt-5 sidebar-fixed animate fadeUp animation-fast">
                                        <div class="sidebar animate fadeUp responce">
                                            <div class="sidebar-content">
                                                <div id="sidebar-list" class="sidebar-menu chat-sidebar list-group">
                                                    <div class="sidebar-list-padding">
                                                        <div class="sidebar-formdata animate fadeUp">
                                                            <div class="formdata-area">
                                                                <input type="text" placeholder="formdata Name" id="formdataform" class="app-filter w-3m-3">
                                                            </div>
                                                        </div>
                                                        <div class="sidebar-content sidebar-chat">
                                                            <div class="chat-list" id="forms">
                                                                <?php foreach ($forms as $form) : ?>
                                                                    <div class="chat-user  animate fadeUp delay-1" onclick="loadform(this.id)" id="<?php echo $form['id']; ?>">
                                                                        <div class="user-section">
                                                                            <div class="row valign-wrapper">
                                                                                <div class="col s2 left media-image online pr-0">
                                                                                    <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $form['cnic_photo']; ?>" alt="" class="circle z-depth-2 responsive-img">
                                                                                </div>
                                                                                <div class="col s10 right">
                                                                                    <p class="m-0  font-weight-700 whitecolour"><?php echo $form['email']; ?> <small class="requestcount">5</small></p>
                                                                                    <p class="m-0 info-text whitecolour"><?php echo $form['email']; ?></p>
                                                                                    <span class="star-timing time whitecolour"><?php echo $form['time_of_visit']; ?></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                <?php endforeach; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-content-area bb-1 ml-1 mt-5 animate fadeUp">
                                        <!-- Chat header -->
                                        <div class="chat-header">
                                            <div class="row valign-wrapper">
                                                <div class="col media-image online pr-0">
                                                    <img src="<?php echo base_url(); ?>assets/uploads/" alt="" class="circle z-depth-2 responsive-img">
                                                </div>
                                                <div class="col">
                                                    <p class="m-0  whitecolour font-weight-700" id='firstname' value=""></p>
                                                    <p class="m-0 chat-text whitecolour truncate" id='email' value=""></p>
                                                </div>
                                            </div>
                                            <span class="option-icon">
                                                <button>delete
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </span>
                                        </div>
                                        <div class="chat-">
                                            <div class="chats">
                                                <div class="chats">
                                                    <div class=" ps ps--active-y">
                                                        <div class="row">
                                                            <div class="col s6">
                                                                <div class="row">
                                                                    <div class="input-field col s12">
                                                                        <h6 class="formview">Full Name: <span id='firstname' value=""></span></h6>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="input-field col s12">
                                                                        <h6 class="formview" id='lastname' value="">Last Name: <span></span></h6>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="input-field col s12">
                                                                        <h6 class="formview" id='cnic' value="">CNIC: <span></span></h6>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="input-field col s12">
                                                                        <h6 class="formview" id='email' value="">Email: <span></span></h6>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="input-field col s12">
                                                                        <h6 class="formview" id='cnic' value="">Phone: <span></span></h6>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col s6">
                                                                <h6 class="formview">Visitor Photo</h6>
                                                                <img class="visitorimg" src="<?php echo base_url(); ?>assets/uploads/" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="chat-footer">
                                                <a class="btn waves-effect waves-light right send" onclick="enter_chat();">Send</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function loadform(productid) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_get_form_details/" + productid,
            success: function(data) {
                var obj = JSON.parse(data);
                alert(data);

                document.getElementById("firstname").value = obj.firstname;
                document.getElementById("lastname").value = obj.lastname;
                document.getElementById("cnic").value = obj.cnic;
                document.getElementById("email").value = obj.email;
                // document.getElementById("cnic_photo").value = obj.cnic_photo;
                // document.getElementById("visitor_photo").value = obj.visitor_photo;
                // document.getElementById("phone").value = obj.phone;
                // document.getElementById("hosted_by_name").value = obj.hosted_by_name;
                // document.getElementById("phone_no_of_the_host").value = obj.phone_no_of_the_host;
                // document.getElementById("date_of_visit").value = obj.date_of_visit;
                // document.getElementById("time_of_visit").value = obj.time_of_visit;
                // document.getElementById("purpose_of_visit").value = obj.purpose_of_visit;
                // document.getElementById("vehicle_no").value = obj.vehicle_no;
                // document.getElementById("visitor_photo").value = obj.visitor_photo;
                // document.getElementById("status").value = obj.status;

            }
        });
    }
</script>