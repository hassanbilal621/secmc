<!-- BEGIN: SideNav-->
<aside class="sidenav-main nav-expanded blackgrade nav-lock nav-collapsible sidenav-fixed hide-on-large-only">



	<!----/////////////////////////////////////////-------------logo-----------///////////////////////////////////////// -->

	<div class="brand-sidebar">
		<h1 class="logo-wrapper">
			<a class="brand-logo darken-1" href="<?php echo base_url(); ?>admin/">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/logo/logo.png" alt="Optv Globle" style="height: 55px;margin: -10% 0 0 35%;"></a>
			<a class="navbar-toggler" href="#">
				<i class="material-icons">radio_button_checked</i>
			</a>
		</h1>
	</div>

	<ul class="sidenav sidenav-collapsible leftside-navigation  blackgrade collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">

		<!----/////////////////////////////////////////-------------Dashboard-----------///////////////////////////////////////// -->
		<li class="navigation-header"><a class="navigation-header-text">	</a>
			
		</li>
		<li class="bold">
			<a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/req.png" alt="" style="height: 30px;margin: 0px 32px -11px 0;">
				<span class="menu-title colour" data-i18n="">Request<small class="requestcount" style="margin: 0px 0.2em;"><?php echo $counter; ?></small></span>
			</a>
		</li>

		<!----/////////////////////////////////////////-------------Staff Management-----------///////////////////////////////////////// -->
		<li class="navigation-header">
			
		</li>
		<li class="bold">
			<a class="collapsible-header waves-effect waves-cyan " href="#">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/staff.png" alt="" style="height: 31px;margin: 0 20px -8px 4px;"><span class="menu-title colour" data-i18n="">Staff</span></a>
			<div class="collapsible-body">
				<ul class="collapsible collapsible-sub" data-collapsible="accordion">
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/addstaff" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span class="colour">Add Staff</span></a></li>
					<li><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managestaff" data-i18n=""><i class="material-icons">keyboard_arrow_right</i><span class="colour">Manage Staff</span></a></li>
				</ul>
			</div>
		</li>

		<!----/////////////////////////////////////////-------------Forms-----------///////////////////////////////////////// -->
		<li class="navigation-header">
			
		</li>

		<li class="bold">
			<a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/forms">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/form.png" alt="" style="height: 30px;margin: 0px 26px -11px 0;">
				<span class="menu-title colour" data-i18n="">Forms</span>
			</a>
		</li>

		<!----/////////////////////////////////////////-------------sign Out-----------///////////////////////////////////////// -->

		<li class="navigation-header">
			
		</li>

		<li class="bold">
			<a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/logout">
				<img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/logout.png" alt="" style="height: 30px;margin: 8px 15px -5px 0px;">
				<span class="menu-title colour" data-i18n="">Sign Out</span>
			</a>
		</li>


	</ul>
	<div class="navigation-background"></div>
	<a class="sidenav-trigger btn-sidenav-toggle  btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons" style="color: white;font-size: 36px;">menu</i></a>
</aside>
<!-- END: SideNav-->