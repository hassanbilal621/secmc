<div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card b2w black br-1">
            <div class="card-content">
               <h4 class="card-title">All Form</h4>
               <a class="waves-effect waves-light  btn  submit right box-shadow-none border-round mr-1 mb-1" href="<?php echo base_url(); ?>users" type="submit" name="action">Add Form
                  <i class="material-icons left">add </i>
               </a>
               <div class="row">
                  <table id="page-length-option" class="display">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>User Name</th>
                           <th>Image</th>
                           <th>Finger Print No</th>
                           <th>User type</th>
                           <th>Action</th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php foreach ($forms as $form) : ?>
                           <tr>
                              <td><?php echo $form['id']; ?></td>
                              <td><?php echo $form['firstname']; ?></td>
                              <td><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $form['cnic_photo']; ?>" width="64px" /> </td>
                              <td><?php echo $form['phone']; ?></td>
                              <td><?php echo $form['hosted_by_name']; ?></td>
                              <td>
                                 <button class="waves-effect waves-light  btn edit box-shadow-none border-round mr-1 mb-1 modal-trigger" onclick="loadforminfo(this.id)" id="<?php echo $form['id']; ?>" type="submit" href="#modal3" name="action">View Form
                                    <i class="material-icons left">visibility</i>
                                 </button>
                                 <a class="waves-effect waves-light  btn  delete box-shadow-none border-round mr-1 mb-1" href="<?php echo base_url(); ?>admin/formdelete/<?php echo $form['id']; ?>" type="submit" name="action">DELETE
                                    <i class="material-icons left">delete_forever</i>
                                 </a>
                              </td>
                              <td></td>
                           </tr>
                        <?php endforeach; ?>
                        </tfoot>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="modal3" class="modal">
   <div class="modal-content" style="background-color: black;">
   </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>
   function loadforminfo(formid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_edit_form_adminmodal/" + formid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>
<script>
   function del(id) {

      swal({
         title: "Are you sure?",
         text: "You will not be able to recover this imaginary file!",
         icon: 'warning',
         dangerMode: true,
         buttons: {
            cancel: 'No, Please!',
            delete: 'Yes, Delete It'
         }
      }).then(function(willDelete) {
         if (willDelete) {
            var html = $.ajax({
               type: "GET",
               url: "<?php echo base_url(); ?>admin/formdelete/" + id,
               // data: info,
               async: false
            }).responseText;

            if (html == "success") {
               $("#delete").html("delete success.");
               return true;

            }
            swal("Poof! Your record has been deleted!", {
               icon: "success",
            });
            setTimeout(location.reload.bind(location), 1000);
         } else {
            swal("Your imaginary file is safe", {
               title: 'Cancelled',
               icon: "error",
            });
         }
      });


   }
</script>