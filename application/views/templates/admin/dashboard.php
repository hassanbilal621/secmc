<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="container">
				<div class="slider">
					<ul class="slides">
						<li>
							<div class="section mt-2" id="blog-list">
								<div class="row">
									<h4 class="deep-purple-text text-darken-3 ">Latest News</h4>
									<?php foreach ($latestnewses as $latestnews) : ?>
										<div class=" caption col s12 m6 l4">
											<div class="card-panel border-radius-6 mt-10 card-animation-1">
												<iframe width="789" class="responsive-img border-radius-8 z-depth-4 image-n-margin" height="444" src="https://www.youtube.com/embed/<?php echo $latestnews['latestnewsurl'];  ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%;height: 300px;"></iframe>
												<h6 class="deep-purple-text text-darken-3 mt-5"><a href="#"><?php echo $latestnews['latestnewstitle']; ?></a></h6>
												<span><?php echo $latestnews['news']; ?></span>
												<div class="row mt-4">
													<div class="col s12 mt-1">
														<span class="pt-2">Date: <?php echo $latestnews['latestnewsdate']; ?> For Web</span>
													</div>
												</div>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<!-- latest News -->
				<div class="section mt-2" id="blog-list">
					<div class="row">
						<h4 class="deep-purple-text text-darken-3 ">Latest News</h4>
						<?php foreach ($latestnewses as $latestnews) : ?>
							<div class="col s12 m6 l4">
								<div class="card-panel border-radius-6 mt-10 card-animation-1">
									<iframe width="789" class="responsive-img border-radius-8 z-depth-4 image-n-margin" height="444" src="https://www.youtube.com/embed/<?php echo $latestnews['latestnewsurl'];  ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%;height: 300px;"></iframe>
									<h6 class="deep-purple-text text-darken-3 mt-5"><a href="#"><?php echo $latestnews['latestnewstitle']; ?></a></h6>
									<span><?php echo $latestnews['news']; ?></span>
									<div class="row mt-4">
										<div class="col s12 mt-1">
											<span class="pt-2">Date: <?php echo $latestnews['latestnewsdate']; ?> For Web</span>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<!-- live link -->
				<div class="row">
					<div class="col s12">
						<h6 class="deep-purple-text text-darken-3 mt-5"><a href="#"><?php echo $live['liveurltitle']; ?></a></h6>
						<iframe src="https://www.youtube.com/embed/<?php echo $live['liveurl']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%;height: 500px;"></iframe>
					</div>
				</div>
				<!-- new -->
				<div class="row">
					<?php foreach ($newses as $news) : ?>
						<div class="col s12 m6 l6">
							<div class="card horizontal border-radius-6">
								<div class="card-image">
									<iframe width="789" class="responsive-img image-n-margin " src="https://www.youtube.com/embed/<?php echo $news['newsurl'];  ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 100%;height: 300px;"></iframe>
								</div>
								<div class="card-stacked">
									<div class="card-content pl-7 pt-7 pr-7 pb-7">
										<h6 class="deep-purple-text text-darken-3 mt-5"><a href="#"><?php echo $news['newstitle']; ?></a></h6>
										<p class="mb-4"><?php echo $news['newsdiscruption']; ?></p>
									</div>
									<div class="card-action pt-4 pb-3">
										<div class="row social-icon">
											<span class="pt-2">
												<?php if ($news['status'] == 'enable') {
														?>
													<p>This Is <span style="color: #14ea14;font-size: large;">Enable</span> For Web</p>
												<?php
													} else { ?>
													<p>This Is <span style="color: red;font-size: large;">Disable</span> For Web</p>
												<?php
													} ?>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>