<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="container">
                <!-- Content Area Starts -->
                <div class="app-email">
                    <div class="content-area content-right">
                        <div class="app-wrapper">
                            <div class="card-default scrollspy border-radius-6 fixed-width" style="border: 2px white solid;">
                                <div class="card-content p-0">
                                    <div class="email-header">
                                        <input type="text" placeholder="Search Mail" id="form_filter" style="width: 100%!important;margin: 0% 0 0 3%;">
                                    </div>
                                    <div class="collection email-collection">
                                        <?php $count = 01 ?>
                                        <?php foreach ($forms as $form) : ?>
                                            <a href="<?php echo base_url(); ?>admin/viewform/<?php echo $form['id']; ?>" id="form" class="collection-item animate fadeUp delay-1" style="background-color: black;">
                                                <span style="margin: 13px 5px 0 0;">0<?php echo $count ?></span>
                                                <div class="list-content">
                                                    <div class="list-title-area">
                                                        <div class="user-media">
                                                            <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $form['cnic_photo']; ?>" alt="" class="circle z-depth-2 responsive-img avtar">
                                                            <div class="list-title"><?php echo $form['firstname']; ?> <?php echo $form['lastname']; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="list-desc">Email : <?php echo $form['email']; ?> <br> Date : <?php echo $form['date_of_visit']; ?> Time:<?php echo $form['time_of_visit']; ?>
                                                    </div>
                                                </div>
                                                <div class="list-right">
                                                    <div class="list-date"><?php echo $form['time_of_visit']; ?>
                                                        <img src="<?php echo base_url(); ?>assets\app-assets\images\gallery\pending.png" alt="" style="max-width: 100px;">
                                                    </div>
                                                </div>
                                            </a>
                                        <?php $count++;
                                        endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal3" class="modal">
    <div class="modal-content" style="background-color: black;">
    </div>
</div>
<script>
    function loadforminfo(formid) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_get_form_details/" + formid,
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal3').modal('open');
            }
        });
    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $("#form_filter").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#form *").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>