
<header class="page-topbar" id="header">
  <div class="navbar navbar-fixed">
    <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark blackgrade">
      <div class="nav-wrapper">
        <ul class="left">
          <li>
            <h1 class="logo-wrapper">
              <a class="brand-logo darken-1 hide-on-med-and-down  pl-20" href="<?php echo base_url(); ?>admin/"><img src="<?php echo base_url(); ?>/assets/app-assets/images/logo/logo.png" alt="Optv Globle" class="logoimg">
                <span class="logo-text hide-on-med-and-down secmc">Sindh Engro Coal Mining Company (SECMC) is operating Pakistan’s first </span></a>
            </h1>
          </li>
        </ul>
        <div class="hide-on-med-and-down">
          <ul class="right hide-on-med-and-down" id="ul-horizontal-nav" data-menu="menu-navigation">
            <li class="bold"><img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/req.png" alt="" class="navicon">
              <a class="ml-07 waves-effect waves-cyan ho_ac navtext" href="<?php echo base_url(); ?>admin/">
              <span class="menu-title    " data-i18n="">Request<small class="notification-badge"><?php echo $counter; ?></small></span></a>
            </li>
            <li class="bold">
              <img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/staff.png" alt="" class="navicon">
              <a class="ml-07 waves-effect waves-cyan ho_ac navtext" href="<?php echo base_url(); ?>admin/managestaff">
                <span class="menu-title " data-i18n="">Manage Staff</span>
              </a>
            </li>
            <li class="bold">
              <img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/form.png" alt="" class="navicon">
              <a class="ml-07 waves-effect waves-cyan ho_ac navtext" href="<?php echo base_url(); ?>admin/forms">
                <span class="menu-title " data-i18n="">Form</span>
              </a>
            </li>
            <li class="bold">
              <img src="<?php echo base_url(); ?>/assets/app-assets/images/icon/logout.png" alt="" class="navicon">
              <a class="ml-07 waves-effect waves-cyan ho_ac navtext" href="<?php echo base_url(); ?>admin/logout">
                <span class="menu-title " data-i18n="">Sign Out</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
</header>