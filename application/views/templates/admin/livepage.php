<div id="main">
	<div class="row">
		<div class="col s12">
			<div class="card br-1">
				<div class="card-content">
					<h4 class="card-title">Live Page</h4>
					<div class="row">
						<div class="col s12">
							<iframe  src="https://www.youtube.com/embed/<?php echo $live['liveurl']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>

					<?php echo form_open_multipart('admin/addlivepage'); ?>
					<div class="row">
						<div class="col s2 right">
							<h6>Date</h6>
							<input type="text" class="datepicker" name="livedate" placeholder=" Type Date" required value="<?php echo $live['livedate']; ?>">
						</div>
					</div>

					<div class="row">
						<div class="col s6">
							<div class="input-field col s12">
								<h6>Title</h6>
								<input id="webname" type="text" name="liveurltitle" placeholder="Type Live URL Title"value="<?php echo $live['liveurltitle']; ?>"> 
							</div>
							<div class="input-field col s12">
								<h6>Live URL</h6>
								<input id="url" type="text" name="liveurl" placeholder="Type Live URL" value="<?php echo $live['liveurl']; ?>">
							</div>
						</div>
						<div class="col s6">
							<div class="input-field col s12">
								<h6>Live Thumnail</h6>
								<input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
							</div>
						</div>
						<div class="input-field col s12">
							<h6>Discruption</h6>
							<textarea id="discruption" type="text" name="liveurldiscruption" maxlength="50" placeholder="Type Discription 1-50" class="discrip"><?php echo $live['liveurldiscruption']; ?></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<div class="input-field col s12">
								<button class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">Go Live
									<i class="material-icons right">live_tv</i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
</div>
</div>

<script>
	$(document).ready(function() {
		$('.datepicker').datepicker();
	});
</script>