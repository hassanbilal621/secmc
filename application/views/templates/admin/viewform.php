<div id="main">
    <div class="row">
        <div class="col s12">
            <div class="container">
                <!-- Content Area Starts -->
                <div class="app-email">
                    <div class="content-area content-right" style="padding-bottom: 3%;">
                        <div class="app-wrapper">
                            <div class="card-default scrollspy border-radius-6 fixed-width" style="border: 2px white solid;">
                                <div class="card-content p-0">
                                    <div class="email-header">
                                        ` <h6>Name : <?php echo $form['firstname']; ?> <?php echo $form['lastname']; ?> Email : <?php echo $form['email']; ?></h6>
                                        <div class="list-right">
                                            <div class="list-date white-text"><?php echo $form['time_of_visit']; ?> </div>
                                        </div>
                                    </div>
                                    <div class="collection email-collection">
                                        <div class="row">
                                            <div class="col s12">
                                                <div class="row">
                                                    <div class="col m12 l6 s12">
                                                        <div class="input-field col s12">
                                                            <h6>CNIC : <?php echo $form['cnic']; ?></h6>
                                                        </div>
                                                    </div>
                                                    <div class="col m12 l6 s12">
                                                        <div class="input-field col s12">
                                                            <h6>Phone : <?php echo $form['phone']; ?>"</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col m12 l6 s12">
                                                        <div class="input-field col s12">
                                                            <h6>CNIC Photo</h6>
                                                            <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $form['cnic_photo']; ?>" style="max-height: 210px;" />
                                                        </div>
                                                    </div>
                                                    <div class="col m12 l6 s12">
                                                        <div class="input-field col s12">
                                                            <h6>Visitor Photo</h6>
                                                            <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $form['visitor_photo']; ?>" style="max-height: 210px;" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <h5 class="normalheading">Hosted form</h5>

                                                <div class="row">
                                                    <div class="col m12 l6 s12">
                                                        <div class="input-field col s12">
                                                            <h6>Hosted by Name : <?php echo $form['hosted_by_name']; ?></h6>
                                                        </div>
                                                    </div>
                                                    <div class="col m12 l6 s12">
                                                        <div class="input-field col s12">
                                                            <h6>Phone No. of the Host : <?php echo $form['phone_no_of_the_host']; ?></h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col m12 l6 s12">
                                                        <div class="input-field col s12">
                                                            <h6>Date of Visit : <?php echo $form['date_of_visit']; ?></h6>
                                                        </div>
                                                    </div>
                                                    <div class="col m12 l6 s12">
                                                        <div class="input-field col s12">
                                                            <h6>Time of Visit : <?php echo $form['time_of_visit']; ?></h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col m12 l6 s12">
                                                        <div class="input-field col s12">
                                                            <h6>Purpose of Visit : <?php echo $form['purpose_of_visit']; ?></h6>
                                                        </div>
                                                    </div>
                                                    <div class="col m12 l6 s12">
                                                        <div class="input-field col s12">
                                                            <h6>Vehicle No : <?php echo $form['vehicle_no']; ?></h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php $count = 01 ?>
                                                <?php foreach ($Visitors as $Visitor) : ?>
                                                    <h5 class="normalheading">Visitor-0<?php echo $count ?></h5>
                                                    <div class="row">
                                                        <div class="col m12 l6 s12">
                                                            <div class="col input-field s12">
                                                                <h6>Visitor Photo</h6>
                                                                <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $Visitor['visitorphoto']; ?>" style="max-height: 210px;" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col m12 l6 s12">
                                                            <div class="input-field col s12">
                                                                <h6>CNIC : <?php echo $Visitor['firstname']; ?></h6>
                                                            </div>
                                                        </div>
                                                        <div class="col m12 l6 s12">
                                                            <div class="input-field col s12">
                                                                <h6>Phone : <?php echo $Visitor['lastname']; ?></h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col m12 l6 s12">
                                                            <div class="input-field col s12">
                                                                <h6>CNIC : <?php echo $Visitor['cnic']; ?></h6>
                                                            </div>
                                                        </div>
                                                        <div class="col m12 l6 s12">
                                                            <div class="input-field col s12">
                                                                <h6>Phone : <?php echo $Visitor['email']; ?></h6>
                                                            </div>
                                                        </div>

                                                    </div>
                                                <?php $count++;
                                                endforeach; ?>
                                            </div>
                                        </div>
                                        <a class="waves-effect waves-light  btn  submit box-shadow-none border-round  mb-1 mr-1 right" href="<?php echo base_url(); ?>admin/approved/<?php echo $form['id']; ?>" type="submit" name="action">Approved
                                            <i class="material-icons left">done_all</i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>